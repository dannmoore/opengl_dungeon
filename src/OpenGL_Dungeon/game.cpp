/*
  File: game.cpp
  Description: Game class constructor, general methods
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#include "game.h"


/*
Game():
	Game class constructor
*/
Game::Game() {
	// Initialize RNG
	const unsigned int seed = static_cast<unsigned int>(time(0));
	rng = new std::mt19937_64(seed);

	// Initialize various game variables
	mGameState = Game::__eGAMESTATE::__MAINVIEW;

	mouseSensitivity = DEFAULT_MOUSE_SENSITIVITY;

	hasFocus = true;

	displayListHandle = -1;
	framesRendered = 0;
	currentFPS = 0;

	clkUpdate.restart();
	clkFPS.restart();
	clkMinimap.restart();
}

/*
~Game():
	Game class destructor
*/

Game::~Game() {
	delete rng; // Cleanup random number generator

	glDeleteLists(displayListHandle, 1);	// Cleanup OpenGL Display list
}


/*
Update():
	Main game loop update event, fires every frame
*/
void Game::mUpdate() {
	// Check input status
	if (hasFocus) mHandleInputStatus();

	switch (mGameState) {
		case Game::__eGAMESTATE::__MAINMENU:
			break;
		case Game::__eGAMESTATE::__MAINVIEW:
			// Update FPS counter
			framesRendered++;
			if (clkFPS.getElapsedTime().asMilliseconds() > 1000) {
				currentFPS = framesRendered;
				framesRendered = 0;
				clkFPS.restart();
			}

			// Update minimap
			mMinimapUpdate();
			break;
		default:
			break;
	}
}


/*
Reset():
	Resets the game state
*/
void Game::Reset() {	
	/*
	LevelData = {
		{1,1,1,1,1,1,1,1,1,1},
		{1,0,0,0,0,0,0,0,0,1},
		{1,0,0,0,0,0,0,0,0,1},
		{1,0,0,0,0,1,0,0,0,1},
		{1,0,0,0,1,1,0,0,0,1},
		{1,0,0,0,0,0,0,0,0,1},
		{1,0,0,0,0,0,0,0,0,1},
		{1,0,0,0,0,0,0,0,0,1},
		{1,0,0,0,0,0,0,0,0,1},
		{1,1,1,1,1,1,1,1,1,1}
	};
	*/

	// Create a new random level
	mGenerateRandomLevel();

	// Create a mesh for the new level
	mCreateLevelMesh();

	// Update the minimap
	mMinimapUpdate();


	// Reset input flags
	bKeyDown_MoveForward = false;
	bKeyDown_MoveBack = false;
	bKeyDown_MoveLeft = false;
	bKeyDown_MoveRight = false;
	bKeyDown_TurnLeft = false;
	bKeyDown_TurnRight = false;
}


/*
Init():
	Perform initialization after the window has been created
*/
void Game::Init() {
	// Initialize minimap
	mMinimapCreate(LEVEL_WIDTH, LEVEL_HEIGHT);

	// Create a test player
	Player p;
	// Default player to upper-left
	p.SetPosition(CUBE_SIZE + CUBE_SIZE / 2.0f, CUBE_SIZE + CUBE_SIZE / 2.0f);
	p.SetHeading(0.0f);
	p.SetPitch(0.0f);
	mPlayers.push_back(p);

	// Save coordinates of center of screen
	windowCenterX = mainWindow->getSize().x / 2;
	windowCenterY = mainWindow->getSize().y / 2;

	// Reset mouse cursor to center of screen	
	sf::Mouse::setPosition(
		{ static_cast<int>(windowCenterX),
		  static_cast<int>(windowCenterY)
		}, *mainWindow
	);

	mainWindow->setMouseCursorVisible(false);

	Reset();
}

