/*
  File: player.h
  Description: Player class header
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#pragma once

#ifndef	__PLAYER_H_
#define	__PLAYER_H_


#include <vector>
#include "helpers.h"
#include "structs.h"


#include <iostream>



class Player {
public:
	Player();
	~Player();

	void Init();

public:
	void SetPosition(float, float);
	void Move(float, float, std::vector<std::vector<_LevelCell>> *, float);
	bool CheckMove(FRECT, std::vector<std::vector<_LevelCell>> *, float);

	void SetHeading(float);
	void SetPitch(float);
	float GetPositionX();
	float GetPositionY();
	float GetHeading();
	float GetPitch();

private:
	float posx;		// Player's current position
	float posy;
	float heading;	// The left/right angle the player is currently facing
	float pitch;	// The up/down angle the player is currently facing
	float size;		// Radius of the size of the player, for collision detection. MUST be greater than 0!
};

#endif // __PLAYER_H_