/*
  File: game_text.cpp
  Description: Game text and font functions
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#include "game.h"



void Game::mLoadFonts() {
	fntDebug.loadFromFile(".\\data\\fnt\\debug.ttf");
}