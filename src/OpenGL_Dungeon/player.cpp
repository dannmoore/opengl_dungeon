/*
  File: player.cpp
  Description: Player class methods
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#include "player.h"

/*
Player():
	Player class constructor
*/
Player::Player() {
	posx = 0;
	posy = 0;
	heading = 0;
	pitch = 0;
	size = 0.5;
}


/*
~Player():
	Player class destructor
*/
Player::~Player() {

}



/*
Init():
	Initializes the player
*/
void Player::Init() {
	posx = 0;
	posy = 0;
	heading = 0;
	pitch = 0;
	size = 0.5;
}


/*
SetPosition():
	Sets the player to the specified position in the world

Params:
	x,y = the position to set the player to in world coordinates
*/
void Player::SetPosition(float newx, float newy) {
	posx = newx;
	posy = newy;
}



/*
CheckMove():
	Checks whether the player can move to the new position

Params:
	r = a bounding box defining the new player's position
	*world = a pointer to the level data
	cellsize = the size of each cell within the level

Returns:
	true if the player can move to the new location, false if not	
*/
bool Player::CheckMove(FRECT r, std::vector<std::vector<_LevelCell>> *world, float cellsize)
{
	int cur_cellx;
	int cur_celly;
	FRECT c;
	FPOINT p;

	// Check each corner of the player's bounding box for collision with a wall

	// Upper-left
	// Get the cell the point is in
	cur_cellx = static_cast<int>(floor(r.left / cellsize));
	cur_celly = static_cast<int>(floor(r.top / cellsize));
	SetRect(&c, cur_cellx * cellsize, cur_celly * cellsize, (cur_cellx * cellsize) + cellsize, (cur_celly * cellsize) + cellsize);
	p.x = r.left;
	p.y = r.top;


	// Sanity check, can only access valid cell indicies
	if (cur_celly >= 0 && cur_celly < (*world).size()
		&& cur_cellx >= 0 && cur_cellx < (*world)[0].size()) 
	{
		if ((*world)[cur_celly][cur_cellx].bWall) { // cell is not empty
			if (bPtWithinRect(&c, p)) {
				return(false);
			}
		}
	}
	else return(false);


	// Upper-right
	// Get the cell the point is in
	cur_cellx = static_cast<int>(floor(r.right / cellsize));
	cur_celly = static_cast<int>(floor(r.top / cellsize));
	SetRect(&c, cur_cellx * cellsize, cur_celly * cellsize, (cur_cellx * cellsize) + cellsize, (cur_celly * cellsize) + cellsize);
	p.x = r.right;
	p.y = r.top;

	// Sanity check, can only access valid cell indicies
	if (cur_celly >= 0 && cur_celly < (*world).size()
		&& cur_cellx >= 0 && cur_cellx < (*world)[0].size())
	{
		if ((*world)[cur_celly][cur_cellx].bWall) { // cell is not empty
			if(bPtWithinRect(&c, p)) {
				return(false);
			}
		}
	}
	else return(false);


	// Lower-left
	// Get the cell the point is in
	cur_cellx = static_cast<int>(floor(r.left / cellsize));
	cur_celly = static_cast<int>(floor(r.bottom / cellsize));
	SetRect(&c, cur_cellx * cellsize, cur_celly * cellsize, (cur_cellx * cellsize) + cellsize, (cur_celly * cellsize) + cellsize);
	p.x = r.left;
	p.y = r.bottom;


	// Sanity check, can only access valid cell indicies
	if (cur_celly >= 0 && cur_celly < (*world).size()
		&& cur_cellx >= 0 && cur_cellx < (*world)[0].size())
	{
		if ((*world)[cur_celly][cur_cellx].bWall) { // cell is not empty
			if (bPtWithinRect(&c, p)) {
				return(false);
			}
		}
	}
	else return(false);


	// Lower-right
	// Get the cell the point is in
	cur_cellx = static_cast<int>(floor(r.right / cellsize));
	cur_celly = static_cast<int>(floor(r.bottom / cellsize));
	SetRect(&c, cur_cellx * cellsize, cur_celly * cellsize, (cur_cellx * cellsize) + cellsize, (cur_celly * cellsize) + cellsize);
	p.x = r.right;
	p.y = r.bottom;


	// Sanity check, can only access valid cell indicies
	if (cur_celly >= 0 && cur_celly < (*world).size()
		&& cur_cellx >= 0 && cur_cellx < (*world)[0].size())
	{
		if ((*world)[cur_celly][cur_cellx].bWall) { // cell is not empty
			if (bPtWithinRect(&c, p)) {
				return(false);
			}
		}
	}
	else return(false);

	return(true);
}


/*
Move():
	Attempts to move the player to the new position

Params:
	newx,newy = the new player's position in world coordinates
	*world = a pointer to the level data
	cellsize = the size of each cell within the level

Returns:
	true if the player can move to the new location, false if not
*/
void Player::Move(float newx, float newy, std::vector<std::vector<_LevelCell>> *world, float cellsize) {
	float testx, testy;
	int cellx, celly;
	FRECT rPlayer;

	testx = newx;
	testy = newy;
	SetRect(&rPlayer, testx - size, testy - size, testx + size, testy + size);

	if (CheckMove(rPlayer, world, cellsize)) {
		posx = testx;
		posy = testy;
		return;
	}

	testx = newx;
	testy = posy;
	SetRect(&rPlayer, testx - size, testy - size, testx + size, testy + size);

	if (CheckMove(rPlayer, world, cellsize)) {
		posx = testx;
		//return;
	}
	else {
		// X move failed, set X to the closest it can be to the wall
		if (posx > newx) { // Moving to the left
			cellx = static_cast<int>(floor(posx / cellsize));
			posx = (cellx * cellsize) + size;
		}
		else if(posx < newx) { // Moving to the right
			cellx = static_cast<int>(floor(posx / cellsize));
			posx = ((cellx + 1) * cellsize) - size;
		}
	}

	testx = posx;
	testy = newy;
	SetRect(&rPlayer, testx - size, testy - size, testx + size, testy + size);

	if (CheckMove(rPlayer, world, cellsize)) {
		posy = testy;
		return;
	}
	else {
		// Y move failed, set Y to the closest it can be to the wall
		if (posy > newy) { // Moving up
			celly = static_cast<int>(floor(posy / cellsize));
			posy = (celly * cellsize) + size;
		}
		else if(posy < newy) { // Moving down
			celly = static_cast<int>(floor(posy / cellsize));
			posy = ((celly + 1) * cellsize) - size;
		}
	}

}



/*
SetHeading():
	Sets the heading angle fo the player

Params:
	angle = the angle to set, in degrees, with 0 degrees pointing north
*/
void Player::SetHeading(float angle) {
	heading = angle;
	// cap heading between 0 and 360 degrees
	if (heading < 0) heading = heading + 360.0f;
	else if (heading >= 360) heading = heading - 360.0f;
}

/*
SetPitch():
	Sets the pitch angle fo the player (up/down)

Params:
	angle = the angle to set, in degrees, with 0 degrees pointing straight ahead
*/
void Player::SetPitch(float angle) {
	pitch = angle;
	// cap pitch between -90 and 90 degrees
	if (pitch < -90) pitch = -90.0f;
	else if (pitch > 90) pitch = 90.0f;
}



/*
GetPositionX():
	Gets the x position of the player

Returns:
	The x position of the player in world coordinates
*/
float Player::GetPositionX() {
	return(posx);
}

/*
GetPositionY():
	Gets the y position of the player

Returns:
	The y position of the player in world coordinates
*/
float Player::GetPositionY() {
	return(posy);
}

/*
GetHeading():
	Gets the heading of the player

Returns:
	The heading angle of the player
*/
float Player::GetHeading() {
	return(heading);
}

/*
GetPositionY():
	Gets the pitch of the player

Returns:
	The pitch angle of the player
*/
float Player::GetPitch() {
	return(pitch);
}