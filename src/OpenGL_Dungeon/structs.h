/*
  File: structs.h
  Description: Struct(s) header
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#pragma once

#ifndef	__STRUCTS_H_
#define	__STRUCTS_H_

#endif


// A struct to hold information about each cell in the level.  Currently only
// storing whether a cell is a wall or not, but can be expanded.
struct _LevelCell {
	bool bWall;		// Is the cell a wall?
};


