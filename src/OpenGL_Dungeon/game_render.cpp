/*
  File: game_render.cpp
  Description: Game rendering functions
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#include "game.h"



/*
SetPerspective():
	A replacement for gluPerspective(), sets the frustum

Params:
	fovY = vertical FOV to set
	aspect = aspect ratio of viewport
	zNear = near clipping plane distance
	zFar = far clipping plane distance
*/
void SetPerspective(GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar)
{
	const GLdouble pi = 3.1415926535897932384626433832795;
	GLdouble fW, fH;

	fH = tan(fovY / 360 * pi) * zNear;
	fW = fH * aspect;

	glFrustum(-fW, fW, -fH, fH, zNear, zFar);
}





/*
mSetWindowHandle():
	Sets the window handle for our renderer to use

Params:
	*w - A pointer to an SF::Window
*/
void Game::mSetWindowHandle(sf::RenderWindow *w) {
	mainWindow = w;
}


/*
mUpdateScalingFactor():
	Updates the scaling factor for your texture buffer, so the drawing
	surface will stretch with the window size
*/
void Game::mUpdateScalingFactor() {
	// Update texture scaling factor
	mScaleFactorX = static_cast<float>(mainWindow->getSize().x) / static_cast<float>(texBuf.getSize().x);
	mScaleFactorY = static_cast<float>(mainWindow->getSize().y) / static_cast<float>(texBuf.getSize().y);
}




/*
mRenderFrame():
	Renders a frame
*/
void Game::mRenderFrame() {
	mainWindow->clear();
	texBuf.clear(sf::Color(0,0,0,0)); // Clear the render buffer with a transparency

	switch (mGameState) {
	case Game::__eGAMESTATE::__MAINMENU:
		break;
	case Game::__eGAMESTATE::__MAINVIEW:
		mRenderWorld();
		mRenderHUD();
		mRenderMinimap();
		break;
	default:
		break;
	}

	mRenderBuf();

	mainWindow->display();
}


/*
mRenderBuf():
	Draws the main texture buffer
*/
void Game::mRenderBuf() {
	texBuf.display(); // Must call display after drawing on a texture or the result isn't oriented correctly

	sf::Texture tmpTex;
	sf::Sprite tmpSprite; // A temporary sprite for drawing
	tmpTex = texBuf.getTexture();
	tmpSprite.setTexture(tmpTex);
	tmpSprite.setTextureRect(sf::IntRect(0, 0, tmpTex.getSize().x, tmpTex.getSize().y));
	tmpSprite.setPosition(0, 0);

	tmpSprite.setScale(
		mScaleFactorX,
		mScaleFactorY
	);


	mainWindow->draw(tmpSprite);
}


/*
mRenderHUD():
	Draws the HUD elements
*/
void Game::mRenderHUD() {
	sf::Text text;
	std::string str;


	text.setFont(fntDebug);
	str = "Player X, Y: " + std::to_string(mPlayers[0].GetPositionX()) + ", " + std::to_string(mPlayers[0].GetPositionY());
	
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	text.setFillColor(sf::Color::Red);
	text.setPosition(sf::Vector2f(8, 0));

	texBuf.draw(text);


	text.setFont(fntDebug);
	str = "Player Heading: " + std::to_string(mPlayers[0].GetHeading());

	text.setString(str);
	text.setCharacterSize(32); // in pixels
	text.setFillColor(sf::Color::Red);
	text.setPosition(sf::Vector2f(8, 24));

	texBuf.draw(text);

	text.setFont(fntDebug);
	str = "Player Pitch: " + std::to_string(mPlayers[0].GetPitch());

	text.setString(str);
	text.setCharacterSize(32); // in pixels
	text.setFillColor(sf::Color::Red);
	text.setPosition(sf::Vector2f(8, 48));

	texBuf.draw(text);

	text.setFont(fntDebug);
	str = "FPS: " + std::to_string(currentFPS);

	text.setString(str);
	text.setCharacterSize(32); // in pixels
	text.setFillColor(sf::Color::Red);
	text.setPosition(sf::Vector2f(texBuf.getSize().x - text.getLocalBounds().width - 8, 0));
	

	texBuf.draw(text);


	text.setFont(fntDebug);
	str = "WASD: Movement, F5: Generate New Map, ESC: Quit";

	text.setString(str);
	text.setCharacterSize(32); // in pixels
	text.setFillColor(sf::Color::Red);
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2) - (text.getLocalBounds().width / 2), texBuf.getSize().y - text.getLocalBounds().height - 16));

	texBuf.draw(text);
}


/*
mRenderMinimap():
	Draws the minimap
*/
void Game::mRenderMinimap() {
	sf::Sprite tmpSprite; // A temporary sprite for drawing
	tmpSprite.setTexture(texMinimap);
	tmpSprite.setTextureRect(sf::IntRect(0, 0, texMinimap.getSize().x, texMinimap.getSize().y));
	tmpSprite.setScale(sf::Vector2f(MINIMAP_SCALE, MINIMAP_SCALE));
	tmpSprite.setPosition(texBuf.getSize().x - (texMinimap.getSize().x * MINIMAP_SCALE) - 16, 96);

	texBuf.draw(tmpSprite);
}





/*
mDrawCeiling():
	Creates a ceiling plane
*/
void Game::mDrawCeiling(int x, int y) {
	glBegin(GL_QUADS);
	glNormal3f(0.0f, -1.0f, 0.0f);
	// Verticies defined in counter-clockwise order
	glTexCoord2f(0.0f, 0.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, CUBE_SIZE, (y * CUBE_SIZE) + CUBE_SIZE);
	glTexCoord2f(1.0f, 0.0f); glVertex3f((x * CUBE_SIZE), CUBE_SIZE, (y * CUBE_SIZE) + CUBE_SIZE);
	glTexCoord2f(1.0f, 1.0f); glVertex3f((x * CUBE_SIZE), CUBE_SIZE, (y * CUBE_SIZE));
	glTexCoord2f(0.0f, 1.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, CUBE_SIZE, (y * CUBE_SIZE));
	glEnd();  
}



/*
mDrawFloor():
	Creates a floor plane
*/
void Game::mDrawFloor(int x, int y) {
	glBegin(GL_QUADS);                
	glNormal3f(0.0f, 1.0f, 0.0f);
	// Verticies defined in counter-clockwise order
	glTexCoord2f(0.0f, 0.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, 0.0f, (y * CUBE_SIZE));
	glTexCoord2f(1.0f, 0.0f); glVertex3f((x * CUBE_SIZE), 0.0f, (y * CUBE_SIZE));
	glTexCoord2f(1.0f, 1.0f); glVertex3f((x * CUBE_SIZE), 0.0f, (y * CUBE_SIZE) + CUBE_SIZE);
	glTexCoord2f(0.0f, 1.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, 0.0f, (y * CUBE_SIZE) + CUBE_SIZE);
	glEnd();  
}

/*
mDrawWall():
	Creates a wall cube

Params:
	x,y = The LevelData index where the wall is located
*/
void Game::mDrawWall(int x, int y) {
	glBegin(GL_QUADS);

	/*
	// We do not need the top or bottom for our wall cubes because the player cannot jump
	// Top
	// Verticies defined in counter-clockwise order
	glNormal3f( 0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, CUBE_SIZE, (y * CUBE_SIZE));
	glTexCoord2f(1.0f, 0.0f); glVertex3f((x * CUBE_SIZE), CUBE_SIZE, (y * CUBE_SIZE));
	glTexCoord2f(1.0f, 1.0f); glVertex3f((x * CUBE_SIZE), CUBE_SIZE, (y * CUBE_SIZE) + CUBE_SIZE);
	glTexCoord2f(0.0f, 1.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, CUBE_SIZE, (y * CUBE_SIZE) + CUBE_SIZE);

	// Bottom
	glNormal3f( 0.0f,-1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, 0.0f, (y * CUBE_SIZE) + CUBE_SIZE);
	glTexCoord2f(1.0f, 0.0f); glVertex3f((x * CUBE_SIZE), 0.0f, (y * CUBE_SIZE) + CUBE_SIZE);
	glTexCoord2f(1.0f, 1.0f); glVertex3f((x * CUBE_SIZE), 0.0f, (y * CUBE_SIZE));
	glTexCoord2f(0.0f, 1.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, 0.0f, (y * CUBE_SIZE));
	*/


	// Front
	glNormal3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, CUBE_SIZE, (y * CUBE_SIZE) + CUBE_SIZE);
	glTexCoord2f(1.0f, 0.0f); glVertex3f((x * CUBE_SIZE), CUBE_SIZE, (y * CUBE_SIZE) + CUBE_SIZE);
	glTexCoord2f(1.0f, 1.0f); glVertex3f((x * CUBE_SIZE), 0.0f, (y * CUBE_SIZE) + CUBE_SIZE);
	glTexCoord2f(0.0f, 1.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, 0.0f, (y * CUBE_SIZE) + CUBE_SIZE);

	// Back
	glNormal3f(0.0f, 0.0f, -1.0f);
	glTexCoord2f(0.0f, 1.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, 0.0f, (y * CUBE_SIZE));
	glTexCoord2f(1.0f, 1.0f); glVertex3f((x * CUBE_SIZE), 0.0f, (y * CUBE_SIZE));
	glTexCoord2f(1.0f, 0.0f); glVertex3f((x * CUBE_SIZE), CUBE_SIZE, (y * CUBE_SIZE));
	glTexCoord2f(0.0f, 0.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, CUBE_SIZE, (y * CUBE_SIZE));

	// Left
	glNormal3f(-1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f); glVertex3f((x * CUBE_SIZE), CUBE_SIZE, (y * CUBE_SIZE) + CUBE_SIZE);
	glTexCoord2f(1.0f, 0.0f); glVertex3f((x * CUBE_SIZE), CUBE_SIZE, (y * CUBE_SIZE));
	glTexCoord2f(1.0f, 1.0f); glVertex3f((x * CUBE_SIZE), 0.0f, (y * CUBE_SIZE));
	glTexCoord2f(0.0f, 1.0f); glVertex3f((x * CUBE_SIZE), 0.0f, (y * CUBE_SIZE) + CUBE_SIZE);

	// Right
	glNormal3f(1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, CUBE_SIZE, (y * CUBE_SIZE));
	glTexCoord2f(1.0f, 0.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, CUBE_SIZE, (y * CUBE_SIZE) + CUBE_SIZE);
	glTexCoord2f(1.0f, 1.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, 0.0f, (y * CUBE_SIZE) + CUBE_SIZE);
	glTexCoord2f(0.0f, 1.0f); glVertex3f((x * CUBE_SIZE) + CUBE_SIZE, 0.0f, (y * CUBE_SIZE));
	glEnd(); 
}


/*
mCreateLevelMesh():
	Creates a display list and populates it with the level mesh
*/
void Game::mCreateLevelMesh() {
	// Delete the old display list, if it exists
	if (displayListHandle != -1) {
		glDeleteLists(displayListHandle, 1);
	}

	// Generate a display list
	displayListHandle = glGenLists(1);

	// Start recording the new display list
	glNewList(displayListHandle, GL_COMPILE);

	// Create level mesh
	glEnable(GL_TEXTURE_2D);

	for (int j = 0; j < LevelData.size(); j++) {
		for (int i = 0; i < LevelData[j].size(); i++) {
			if (LevelData[j][i].bWall != 0) {
				sf::Texture::bind(&texWall);
				mDrawWall(i, j);
			}
			else { // No wall, draw the visible floor
				sf::Texture::bind(&texFloor);
				mDrawFloor(i, j);
				sf::Texture::bind(&texCeiling);
				mDrawCeiling(i, j);
			}
		}
	}

	// End recording the display list
	glEndList();
}


/*
mDrawLevelMesh():
	Draws the level mesh from the display list
*/
void Game::mDrawLevelMesh() {
	// If we wanted to do further modification to the mesh position, save
	// the matrix
	//glPushMatrix();
	//glTranslatef(...); // For example

	// Call the display list to draw the level mesh
	glCallList(displayListHandle);

	// Restore the old matrix
	//glPopMatrix();
}




/*
mRenderWorld():
	Renders the world
*/
void Game::mRenderWorld() {
	mainWindow->popGLStates(); // Restore previously saved opengl states

	// OpenGL settings
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glMatrixMode(GL_PROJECTION); // Operate on the projection matrix
	glLoadIdentity(); // Reset

	// Enable perspective projection
	SetPerspective(VFOV, (float)mainWindow->getSize().x / (float)mainWindow->getSize().y, 0.1f, DRAW_DISTANCE);

	glMatrixMode(GL_MODELVIEW); // Operate on the model matrix
	
	glLoadIdentity();	// Reset

	// Rotate and translate the world around the player
	glRotatef(mPlayers[0].GetPitch(), 1.0f, 0.0f, 0.0f);
	glRotatef(mPlayers[0].GetHeading(), 0.0f, 1.0f, 0.0f);
	glTranslatef(-mPlayers[0].GetPositionX(), -1.0f, -mPlayers[0].GetPositionY());


	glShadeModel(GL_SMOOTH);
	glEnable(GL_TEXTURE_2D);

	// Draw the level mesh
	mDrawLevelMesh();

	mainWindow->pushGLStates(); // Save opengl states before making more SFML calls
}