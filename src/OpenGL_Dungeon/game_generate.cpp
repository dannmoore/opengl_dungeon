/*
  File: game_generate.cpp
  Description: Game level generation
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#include "game.h"

// Parameters for creating random levels
#define __PARAM_NUMMAPCELLS	8			// Number of map cells to use
#define __PARAM_MINROOMS 24				// Minimum number of rooms to create
#define __PARAM_MAXROOMS 32				// Maximum number of rooms to create
#define __PARAM_MINROOMSIZE 3			// Minimum room size
#define __PARAM_MAXROOMSIZE 10			// Maximum room size
#define __PARAM_NUM_EXTRA_CONNECTIONS 0	// Number of extra hallways to create between rooms





struct _mapcell
{
	BOOL room_present;
	BOOL been_connected;
	int connected_cell_x;
	int connected_cell_y;
	POINT pos;
	POINT roomsize;
};

/*
mGenerateRandomLevel():
	Generate a new random level
*/
void Game::mGenerateRandomLevel() {
	bool cont;

	_mapcell mapcell[__PARAM_NUMMAPCELLS][__PARAM_NUMMAPCELLS];
	int newcellx;
	int newcelly;
	int cellsizex = LEVEL_WIDTH / __PARAM_NUMMAPCELLS;
	int cellsizey = LEVEL_HEIGHT / __PARAM_NUMMAPCELLS;


	// Clear level data
	LevelData.clear();
	std::vector<_LevelCell> v(LEVEL_WIDTH);
	for (int i = 0; i < LEVEL_HEIGHT; i++) {
		LevelData.push_back(v);
	}

	// Fill level with solid walls, rooms will be carved out
	for (int j = 0; j < LEVEL_HEIGHT; j++) {
		for (int i = 0; i < LEVEL_WIDTH; i++) {
			LevelData[j][i].bWall = true;
		}
	}

	

	// Next, we divide the map into cells
	for (int i = 0; i < __PARAM_NUMMAPCELLS; i++)
	{
		for (int j = 0; j < __PARAM_NUMMAPCELLS; j++)
		{
			mapcell[i][j].room_present = FALSE;
			mapcell[i][j].been_connected = FALSE;
			mapcell[i][j].connected_cell_x = -1;
			mapcell[i][j].connected_cell_y = -1;
		}
	}


	// Choose a starting cell
	int startx = mGetRandomInt(0, __PARAM_NUMMAPCELLS - 1);
	int starty = mGetRandomInt(0, __PARAM_NUMMAPCELLS - 1);
	mapcell[startx][starty].room_present = TRUE;


	// Now, determine the number of additional rooms to add
	int additional_rooms = mGetRandomInt(__PARAM_MINROOMS, __PARAM_MAXROOMS) - 1; //-1 because we already created the starting room

	for (int i = 0; i < additional_rooms; i++)
	{
		// Select a cell to place this room in
		do
		{
			newcellx = mGetRandomInt(0, __PARAM_NUMMAPCELLS - 1);
			newcelly = mGetRandomInt(0, __PARAM_NUMMAPCELLS - 1);
		} while (mapcell[newcellx][newcelly].room_present == TRUE);

		mapcell[newcellx][newcelly].room_present = TRUE;
	}


	// Next, connect the cells together
	int failsafe;
	for (int i = 0; i < __PARAM_NUMMAPCELLS; i++)
	{
		for (int j = 0; j < __PARAM_NUMMAPCELLS; j++)
		{
			if (mapcell[i][j].room_present && !mapcell[i][j].been_connected)
			{
				// Randomly connect to another room
				failsafe = 0; // Failsafe in case all rooms are already connected
				do
				{
					newcellx = mGetRandomInt(0, __PARAM_NUMMAPCELLS - 1);
					newcelly = mGetRandomInt(0, __PARAM_NUMMAPCELLS - 1);

					if (i != newcellx || j != newcelly) // Can't connect to itself
					{
						if (mapcell[newcellx][newcelly].room_present)
						{
							if (mapcell[newcellx][newcelly].connected_cell_x == -1) // new cell is not connected to anything
							{
								// create a link between the cells
								mapcell[i][j].connected_cell_x = newcellx;
								mapcell[i][j].connected_cell_y = newcelly;

								// Mark both cells as having been connected
								mapcell[i][j].been_connected = TRUE;
								mapcell[newcellx][newcelly].been_connected = TRUE;
							}
						}
					}
					failsafe++;
				} while (mapcell[i][j].connected_cell_x == -1 && failsafe < (__PARAM_NUMMAPCELLS * __PARAM_NUMMAPCELLS * 2));
				
				// Note: if we broke out as a result of the failsafe, we could decide to leave it as
				// an orphaned room or connect it to any room regardless by writing code here...

			}

		}
	}




	// Next, generate rooms in each cell
	for (int i = 0; i < __PARAM_NUMMAPCELLS; i++)
	{
		for (int j = 0; j < __PARAM_NUMMAPCELLS; j++)
		{
			if (mapcell[i][j].room_present)
			{
				mapcell[i][j].roomsize.x = mGetRandomInt(__PARAM_MINROOMSIZE, __PARAM_MAXROOMSIZE);
				mapcell[i][j].roomsize.y = mGetRandomInt(__PARAM_MINROOMSIZE, __PARAM_MAXROOMSIZE);

				// Choose a starting point to draw from based on grid size and room size
				mapcell[i][j].pos.x = (i * cellsizex) + mGetRandomInt(0, cellsizex - mapcell[i][j].roomsize.x);
				mapcell[i][j].pos.y = (j * cellsizey) + mGetRandomInt(0, cellsizey - mapcell[i][j].roomsize.y);

				// Finally, carve the room into our dungeon
				mGenerateCreateRoom(mapcell[i][j].pos.x, mapcell[i][j].pos.y,
					mapcell[i][j].pos.x + mapcell[i][j].roomsize.x, mapcell[i][j].pos.y + mapcell[i][j].roomsize.y);

			}
		}
	}


	// Next, connect each room together
	POINT _room1;
	POINT _room2;
	for (int i = 0; i < __PARAM_NUMMAPCELLS; i++)
	{
		for (int j = 0; j < __PARAM_NUMMAPCELLS; j++)
		{
			if (mapcell[i][j].room_present)
			{
				if (mapcell[i][j].connected_cell_x != -1)
				{
					// Choose a random point inside the current room to start from (dont use outside edge of room)
					_room1.x = mapcell[i][j].pos.x + mGetRandomInt(1, mapcell[i][j].roomsize.x - 2);
					_room1.y = mapcell[i][j].pos.y + mGetRandomInt(1, mapcell[i][j].roomsize.y - 2);

					// Choose a random point inside the connected room to end at (dont use outside edge of room)
					_room2.x = mapcell[mapcell[i][j].connected_cell_x][mapcell[i][j].connected_cell_y].pos.x + mGetRandomInt(1, mapcell[mapcell[i][j].connected_cell_x][mapcell[i][j].connected_cell_y].roomsize.x - 2);
					_room2.y = mapcell[mapcell[i][j].connected_cell_x][mapcell[i][j].connected_cell_y].pos.y + mGetRandomInt(1, mapcell[mapcell[i][j].connected_cell_x][mapcell[i][j].connected_cell_y].roomsize.y - 2);

					mGenerateCreateHall(_room1, _room2);
				}
			}
		}
	}



	// Now let's create a few extra connections between cells for flavor
	int secondcellx;
	int secondcelly;
	for (int q = 0; q < __PARAM_NUM_EXTRA_CONNECTIONS; q++)
	{
		failsafe = 0; // Failsafe in case there are no rooms
		do
		{
			newcellx = mGetRandomInt(0, __PARAM_NUMMAPCELLS - 1);
			newcelly = mGetRandomInt(0, __PARAM_NUMMAPCELLS - 1);
			failsafe++;
		} while (!mapcell[newcellx][newcelly].room_present && failsafe < (__PARAM_NUMMAPCELLS * __PARAM_NUMMAPCELLS * 2));
		if (failsafe >= (__PARAM_NUMMAPCELLS * __PARAM_NUMMAPCELLS * 2)) break; // Failed to find a room, break out
		else
		{
			// Get a random point in the room
			_room1.x = mapcell[newcellx][newcelly].pos.x + mGetRandomInt(1, mapcell[newcellx][newcelly].roomsize.x - 2);
			_room1.y = mapcell[newcellx][newcelly].pos.y + mGetRandomInt(1, mapcell[newcellx][newcelly].roomsize.y - 2);

			// Get a random cell to connect to, regardless of whether it has a room or not
			secondcellx = mGetRandomInt(0, __PARAM_NUMMAPCELLS - 1);
			secondcelly = mGetRandomInt(0, __PARAM_NUMMAPCELLS - 1);
			if (mapcell[secondcellx][secondcelly].room_present)
			{
				// Connect to a point within the room
				_room2.x = mapcell[secondcellx][secondcelly].pos.x + mGetRandomInt(1, mapcell[secondcellx][secondcelly].roomsize.x - 2);
				_room2.y = mapcell[secondcellx][secondcelly].pos.y + mGetRandomInt(1, mapcell[secondcellx][secondcelly].roomsize.y - 2);

				mGenerateCreateHall(_room1, _room2);
			}
			else
			{
				// Connect to any point in the cell, this will give a nice dead-end hallway
				_room2.x = (secondcellx * cellsizex) + mGetRandomInt(0, cellsizex - 1);
				_room2.y = (secondcelly * cellsizey) + mGetRandomInt(0, cellsizey - 1);
				mGenerateCreateHall(_room1, _room2);
			}
		}
	}


	// Default player to upper-left
	mPlayers[0].SetPosition(CUBE_SIZE / 0.5f, CUBE_SIZE / 0.5f);
	mPlayers[0].SetHeading(0);

	// Restart the player in the first available empty space
	cont = true;
	for (int j = 0; j < LEVEL_HEIGHT; j++)
	{
		for (int i = 0; i < LEVEL_WIDTH; i++)
		{
			if (LevelData[j][i].bWall == false)
			{
				mPlayers[0].SetPosition((i * CUBE_SIZE) + (CUBE_SIZE / 2.0f), (j * CUBE_SIZE) + (CUBE_SIZE / 2.0f));
				cont = false;
				break;
			}
			if (!cont) break;
		}
	}
}


/*
mGenerateCreateHall():
	Creates a hallway from one point to another

Params:
	p1,p2 = Starting and ending point for the hallway
*/
void Game::mGenerateCreateHall(POINT p1, POINT p2)
{
	int endx;
	int endy;

	// Determine direction of the cells and begin drawing empty tiles
	// until reaching the destination
	// Choose whether to go up/down or left/right first for more randomness
	if (mGetRandomInt(0, 1) == 1)
	{ // Do vertical first
		// Draw empty cells from the first point over to the second point, making a 90 degree turn
		if (p1.y > p2.y)
		{
			endy = p1.y;
			for (int y = p2.y; y <= p1.y; y++)
			{
				if (p1.x > 0 && p1.x < LEVEL_WIDTH - 1
					&& y > 0 && y < LEVEL_HEIGHT - 1) { // Don't allow halls to go out of bounds
					LevelData[y][p2.x].bWall = false;
				}
			}
		}
		else if (p1.y < p2.y)
		{
			endy = p2.y;
			for (int y = p1.y; y <= p2.y; y++)
			{
				if (p1.x > 0 && p1.x < LEVEL_WIDTH - 1
					&& y > 0 && y < LEVEL_HEIGHT - 1) { // Don't allow halls to go out of bounds
					LevelData[y][p1.x].bWall = false;
				}
			}
		}
		else endy = p1.y; // both same				


		if (p1.x > p2.x)
		{
			for (int x = p2.x; x <= p1.x; x++)
			{
				if (x > 0 && x < LEVEL_WIDTH - 1
					&& p1.y > 0 && p1.y < LEVEL_HEIGHT - 1) { // Don't allow halls to go out of bounds
					LevelData[endy][x].bWall = false;
				}
			}
		}
		else if (p1.x < p2.x)
		{
			for (int x = p1.x; x <= p2.x; x++)
			{
				if (x > 0 && x < LEVEL_WIDTH - 1
					&& p1.y > 0 && p1.y < LEVEL_HEIGHT - 1) { // Don't allow halls to go out of bounds
					LevelData[endy][x].bWall = false;
				}
			}
		}
	}
	else
	{ // Do horizontal first
		// Draw empty cells from the first point over to the second point, making a 90 degree turn
		if (p1.x > p2.x)
		{
			endx = p1.x;
			for (int x = p2.x; x <= p1.x; x++)
			{
				if (x > 0 && x < LEVEL_WIDTH - 1
					&& p1.y > 0 && p1.y < LEVEL_HEIGHT - 1) { // Don't allow halls to go out of bounds
					LevelData[p2.y][x].bWall = false;
				}
			}
		}
		else if (p1.x < p2.x)
		{
			endx = p2.x;
			for (int x = p1.x; x <= p2.x; x++)
			{
				if (x > 0 && x < LEVEL_WIDTH - 1
					&& p1.y > 0 && p1.y < LEVEL_HEIGHT - 1) { // Don't allow halls to go out of bounds
					LevelData[p1.y][x].bWall = false;
				}
			}
		}
		else endx = p1.x; // both same

		if (p1.y > p2.y)
		{
			for (int y = p2.y; y <= p1.y; y++)
			{
				if (p1.x > 0 && p1.x < LEVEL_WIDTH - 1
					&& y > 0 && y < LEVEL_HEIGHT - 1) { // Don't allow halls to go out of bounds
					LevelData[y][endx].bWall = false;
				}
			}
		}
		else if (p1.y < p2.y)
		{
			for (int y = p1.y; y <= p2.y; y++)
			{
				if (p1.x > 0 && p1.x < LEVEL_WIDTH - 1
					&& y > 0 && y < LEVEL_HEIGHT - 1) { // Don't allow halls to go out of bounds
					LevelData[y][endx].bWall = false;
				}
			}
		}
	} // horizontal first drawing
}



/*
mGenerateCreateRoom():
	Creates a room at the specified point and specified dimensions

Params:
	x,y = Upper-left starting point of the room to create
	width,height = Dimensions of the room to create
*/
void Game::mGenerateCreateRoom(int x, int y, int width, int height)
{
	for (int i = x; i < width; i++)
	{
		for (int j = y; j < height; j++)
		{
			if (i > 0 && i < LEVEL_WIDTH - 1
				&& j > 0 && j < LEVEL_HEIGHT - 1) { // Don't allow rooms to extend into unbreakable border
				LevelData[j][i].bWall = false;
			}
		}
	}
}


