/*
  File: game_minimap.cpp
  Description: Game minimap functionality
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#include "game.h"


/*
mMinimapCreate():
	Creates the minimap textures

Params:
	width,height = dimensions of minimap to create in pixels
*/
void Game::mMinimapCreate(unsigned int width, unsigned int height) {
	texMinimap.create(width, height);  // Create a texture of the specified size
	imgMinimap.create(width, height, sf::Color(sf::Color::Black));  // Create the updatable image, all black

	bMinimapFlashPlayer = false;
}




/*
mMinimapUpdate():
	Updates the minimap based on the level data
*/
void Game::mMinimapUpdate() {
	// Plot level data onto minimap
	for (int j = 0; j < LevelData.size(); j++) {
		for (int i = 0; i < LevelData[j].size(); i++) {
			if (LevelData[j][i].bWall != 0) {
				imgMinimap.setPixel(i, j, sf::Color(0,0,0,0));
			}
			else {
				imgMinimap.setPixel(i, j, sf::Color::Red);
			}
		}
	}

	// Flash the player position on a timer
	if (clkMinimap.getElapsedTime().asMilliseconds() > 250) {
		bMinimapFlashPlayer = !bMinimapFlashPlayer;
		clkMinimap.restart();
	}


	// Draw player onto minimap
	int cellx = static_cast<int>(floor(mPlayers[0].GetPositionX() / CUBE_SIZE));
	int celly = static_cast<int>(floor(mPlayers[0].GetPositionY() / CUBE_SIZE));
	if (bMinimapFlashPlayer) {
		imgMinimap.setPixel(cellx, celly, sf::Color::White);
	}
	else {
		imgMinimap.setPixel(cellx, celly, sf::Color::Blue);
	}

	texMinimap.update(imgMinimap);
}

