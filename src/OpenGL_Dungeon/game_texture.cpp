/*
  File: game_texture.cpp
  Description: Game texture functions
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#include "game.h"


/*
mLoadTextures():
	Loads textures from the file system into memory
*/
void Game::mLoadTextures() {
	texWall.loadFromFile(".\\data\\img\\wall_texture.png");
	texFloor.loadFromFile(".\\data\\img\\floor_texture.png");
	texCeiling.loadFromFile(".\\data\\img\\ceiling_texture.png");

	texBuf.create(1920, 1080);	// Create our buffer to draw on
}


