/*
  File: helpers.h
  Description: Helper functions header
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#pragma once

#ifndef	__HELPERS_H_
#define	__HELPERS_H_


// constants
#define __PI__ 3.141592654f
#define __SQRT2__ 1.414213562f


// macros
#define __SAFE_DELETE(p) 		{ if(p) { delete (p); (p)=NULL; } }			// delete macro
#define __SAFE_DELETE_ARRAY(p) 	{ if(p) { delete [] (p); (p)=NULL; } }		// delete array macro

#define __DEG2RAD(d)			(d * __PI__ / 180.0f)
#define __RAD2DEG(r)			(r * 180.0f / __PI__)


typedef struct __FPoint {
	float x;
	float y;
} FPOINT;

typedef struct __FRect {
	float left;
	float top;
	float right;
	float bottom;
} FRECT;



/*
SetRect():
	Sets the given rect to the given values

Params:
	*r = Pointer to rect
	left, top, right, bottom = dimensions of rect to be set
*/
inline void SetRect(FRECT * r, float left, float top, float right, float bottom) {
	r->left = left;
	r->top = top;
	r->right = right;
	r->bottom = bottom;
}


/*
SetPoint():
	Sets the given point to the given values

Params:
	*p = Pointer to point
	x,y = values of point to be set
*/
inline void SetPoint(FPOINT * p, float x, float y) {
	p->x = x;
	p->y = y;
}


/*
bPtInRect():
	Checks for the given point inside the given rect

Params:
	x,y = point to check
	r = rect to check

Returns:
	true if the given point is inside the given rect
*/
inline bool bPtinRect(FRECT * r, FPOINT p) {
	if (p.x >= r->left
		&& p.x <= r->right
		&& p.y >= r->top
		&& p.y <= r->bottom) {
		return(true);
	}
	else {
		return(false);
	}
}


/*
bPtWithinRect():
	Checks for the given point inside the given rect, but not including
	the border lines

Params:
	x,y = point to check
	r = rect to check

Returns:
	true if the given point is within the given rect
*/
inline bool bPtWithinRect(FRECT * r, FPOINT p) {
	if (p.x > r->left
		&& p.x < r->right
		&& p.y > r->top
		&& p.y < r->bottom) {
		return(true);
	}
	else {
		return(false);
	}
}

/*
bRectInRect():
	Checks for an intersection between the given rects

Params:
	*r1, *r2 = pointers to the rects to check

Returns:
	true if the given rects intersect
*/
inline bool bRectInRect(FRECT *r1, FRECT *r2) {
	FPOINT _pt;

	_pt.x = r1->left; 
	_pt.y = r1->top;
	if (bPtinRect(r2, _pt)) {
		return(true);
	}

	_pt.x = r1->right; 
	_pt.y = r1->top;
	if (bPtinRect(r2, _pt)) {
		return(true);
	}

	_pt.x = r1->left; 
	_pt.y = r1->bottom;
	if (bPtinRect(r2, _pt)) {
		return(true);
	}

	_pt.x = r1->right; 
	_pt.y = r1->bottom;
	if (bPtinRect(r2, _pt)) {
		return(true);
	}

	return(false);
}




#endif // __HELPERS_H_