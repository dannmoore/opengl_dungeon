/*
  File: game.h
  Description: Game class header
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#ifndef	__GAME_H_
#define	__GAME_H_


#include <random>
#include <ctime>
#include <iostream>
#include <list>
#include <vector>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/OpenGL.hpp> // Note: Windows 8.1 SDK will throw an error if compiler option "/permissive-" is set while including this!

#include "player.h"
#include "helpers.h"
#include "structs.h"


// Game Constants and Defines
#define FULLSCREEN true				// If true, start game in borderless windowed mode, if false start game in a regular window

#define DEFAULT_MOUSE_SENSITIVITY 0.5f	// Default angle change per mousex

#define LEVEL_WIDTH 100					// Width and height (in cells) of the level
#define LEVEL_HEIGHT 100

#define CUBE_SIZE 2.0f					// Size of each cell/cube drawn
#define VFOV 45.0f						// Vertical FOV of the view
#define DRAW_DISTANCE 200.0f

#define MINIMAP_SCALE 4.0f				// Embiggen the minimap by this factor

#define MOVEMENT_SPEED 0.1f				// How many units to move per game tick
#define TURN_SPEED 1.0f					// How many degrees to turn per game tick when using keyboard




class Game {
public:
	Game();
	~Game();

	void Init();
	void Reset();



	// Utility
public:
	std::mt19937_64 *rng;	// Random number generator
	int mGetRandomInt(int, int);
	float mVFOVtoHFOV(float, float, float);	// Conversion from vertical FOV to horizontal FOV

private:



	// Rendering
public:
	void mSetWindowHandle(sf::RenderWindow *);
	void mUpdateScalingFactor();
	void mRenderFrame();

	void mRenderBuf();
	void mRenderWorld();
	void mRenderHUD();
	void mRenderMinimap();
	void mDrawWall(int, int);
	void mDrawFloor(int, int);
	void mDrawCeiling(int, int);
	void mCreateLevelMesh();
	void mDrawLevelMesh();

	float mScaleFactorX;	// Scaling factor for the render buffer vs window size (only applicable to HUD elements)
	float mScaleFactorY;


private:
	sf::RenderWindow *mainWindow;

	int windowCenterX;
	int windowCenterY;

	bool hasFocus;

	int displayListHandle;


	// Fonts and text
public:
	void mLoadFonts();

private:
	sf::Font fntDebug;	// Debug font





	// Textures
public:
	void mLoadTextures();


private:
	sf::Texture texWall;
	sf::Texture texFloor;
	sf::Texture texCeiling;
	sf::Texture texMinimap;
	sf::Image imgMinimap;

	sf::RenderTexture texBuf;



	// Minimap
public:
	void mMinimapCreate(unsigned int, unsigned int);
	void mMinimapUpdate();

private:
	sf::Clock clkMinimap;		// A timer to animate the minimap
	bool bMinimapFlashPlayer;	// A flag to flash the player's position on minimap



	// Input
public:
	void mHandleInput(sf::Event *);
	void mInputKeyDown(sf::Keyboard::Key);
	void mInputKeyUp(sf::Keyboard::Key);
	void mMouseButtonPressed(sf::Mouse::Button);
	void mMouseMoved(int, int);
	void mHandleInputStatus();


private:
	float mouseSensitivity;
	int prev_mouseX;	// Save previous mouse position
	int prev_mouseY;

	bool bKeyDown_MoveForward;
	bool bKeyDown_MoveBack;
	bool bKeyDown_MoveLeft;
	bool bKeyDown_MoveRight;
	bool bKeyDown_TurnLeft;
	bool bKeyDown_TurnRight;


	// Gameplay
public:
	enum __eGAMESTATE {
		__MAINMENU = 0,
		__MAINVIEW = 1,
	};

	__eGAMESTATE mGameState;


	std::vector<std::vector<_LevelCell>> LevelData;	// The actual level data
	std::vector<Player> mPlayers;	// Player data
	
	void mUpdate();
	void mGenerateRandomLevel();
	void mGenerateCreateRoom(int, int, int, int);
	void mGenerateCreateHall(POINT, POINT);



private:
	sf::Clock clkUpdate;	// A timer to update the game




	// FPS counter
public:

private:
	sf::Clock clkFPS;
	int framesRendered;
	int currentFPS;

};



#endif // __GAME_H_
