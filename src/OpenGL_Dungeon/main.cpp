/*
  File: main.cpp
  Description: Main function, setup game object and main loop
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#include "game.h"
#include "helpers.h"


Game *g;


int main()
{
	// Create game object and initialize
	g = new Game();
	g->mLoadTextures();
	g->mLoadFonts();

	// SFML settings for the window
	sf::ContextSettings settings;
	settings.depthBits = 24;
	settings.stencilBits = 8;
	settings.antialiasingLevel = 4;
	settings.majorVersion = 4;
	settings.minorVersion = 0;

	// Create the window
	#if (FULLSCREEN)
		sf::RenderWindow window(sf::VideoMode::getDesktopMode(), "DungeonRun", sf::Style::None, settings);
	#else
		sf::RenderWindow window(sf::VideoMode(1280, 720), "DungeonRun", sf::Style::Titlebar | sf::Style::Close | sf::Style::Resize, settings);
	#endif

	window.setFramerateLimit(60);

	g->mSetWindowHandle(&window);
	g->mUpdateScalingFactor();
	
	g->Init();

	/* 
	// Debug, show the actual window settings as created
	settings = window.getSettings();
	std::cout << "depth bits:" << settings.depthBits << std::endl;
	std::cout << "stencil bits:" << settings.stencilBits << std::endl;
	std::cout << "antialiasing level:" << settings.antialiasingLevel << std::endl;
	std::cout << "version:" << settings.majorVersion << "." << settings.minorVersion << std::endl;
	*/

	// Main loop
	while (window.isOpen())
	{
		sf::err().rdbuf(NULL);	// This call is here to suppress DirectInput errors from SFML regarding joystick input, may need to dig into SFML source code to find a solution

		sf::Event event;
		while (window.pollEvent(event))
		{
			// system events
			if (event.type == sf::Event::Closed)
				window.close();

			// window events
			if (event.type == sf::Event::Resized) {
				g->mHandleInput(&event);
			}

			else if (event.type == sf::Event::GainedFocus) {
				g->mHandleInput(&event);
			}
			
			else if (event.type == sf::Event::LostFocus) {
				g->mHandleInput(&event);
			}

			// keyboard events
			else if (event.type == sf::Event::KeyPressed) {
				g->mHandleInput(&event);
			}
			else if (event.type == sf::Event::KeyReleased) {
				g->mHandleInput(&event);
			}

			// mouse events
			else if (event.type == sf::Event::MouseLeft) {
				g->mHandleInput(&event);
			}
			else if (event.type == sf::Event::MouseMoved) {
				g->mHandleInput(&event);
			}
			else if (event.type == sf::Event::MouseButtonPressed) {
				g->mHandleInput(&event);
			}
		}


		g->mUpdate();
		g->mRenderFrame();

		// TODO: Handle errors here
		sf::err().rdbuf(NULL);
	}


	__SAFE_DELETE(g);

	return 0;
}
