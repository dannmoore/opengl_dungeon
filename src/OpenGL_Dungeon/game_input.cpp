/*
  File: game_input.cpp
  Description: Game class input methods
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#include "game.h"



/*
mHandleInput():
	Handles SFML input events

Params:
	*e = SFML event
*/
void Game::mHandleInput(sf::Event *e) {
	sf::Vector2f newsize;
	sf::Vector2f newcenter;

	switch (e->type) {
		// Keyboard events
		case sf::Event::KeyPressed: // Key down or repeating
			mInputKeyDown(e->key.code);
			break;
		case sf::Event::KeyReleased: // Key up
			mInputKeyUp(e->key.code);
			break;

			// Mouse events
		case sf::Event::MouseMoved: // Mouse moved within the window
			mMouseMoved(e->mouseMove.x, e->mouseMove.y);
			break;
		case sf::Event::MouseButtonPressed: // Mouse button pressed
			mMouseButtonPressed(e->mouseButton.button);
			break;
		case sf::Event::MouseLeft: // Mouse left window
			break;

			// Window events
		case sf::Event::Resized: // Window was resized
			// Set the size of the default view so things get stretched appropriately
			newsize.x = static_cast<float>(mainWindow->getSize().x); newsize.y = static_cast<float>(mainWindow->getSize().y);
			newcenter.x = newsize.x / 2.0f; newcenter.y = newsize.y / 2.0f;
			mainWindow->setView(sf::View(newcenter, newsize));

			// Update the scaling factor for SFML drawing (mostly HUD)
			mUpdateScalingFactor();

			// Update the OpenGL viewport
			glViewport(0, 0, mainWindow->getSize().x, mainWindow->getSize().y);

			// Update coordinates of center of screen
			windowCenterX = mainWindow->getSize().x / 2;
			windowCenterY = mainWindow->getSize().y / 2;
		
			// Reset mouse cursor to center of screen
			sf::Mouse::setPosition(
				{ static_cast<int>(windowCenterX),
				  static_cast<int>(windowCenterY)
				}
			);
			break;
		
		case sf::Event::GainedFocus: // Window has gained focus
			hasFocus = true;

			// Save coordinates of center of screen
			windowCenterX = mainWindow->getPosition().x + (mainWindow->getSize().x / 2);
			windowCenterY = mainWindow->getPosition().y + (mainWindow->getSize().y / 2);

			// Reset mouse cursor to center of screen
			sf::Mouse::setPosition(
				{ static_cast<int>(windowCenterX),
				  static_cast<int>(windowCenterY)
				}
			);

			mainWindow->setMouseCursorVisible(false);
			break;

		case sf::Event::LostFocus: // Window has lost focus
			hasFocus = false;
			mainWindow->setMouseCursorVisible(true);
			break;
		
			default:
				break;
	} // switch()
}



/*
mInputKeyDown():
	Handles key down input events

Params:
	k = SFML key pressed
*/
void Game::mInputKeyDown(sf::Keyboard::Key k) {

	switch (mGameState) {
	case Game::__eGAMESTATE::__MAINMENU:
		break;
	case Game::__eGAMESTATE::__MAINVIEW:
		switch (k) {
			case sf::Keyboard::Escape:
				mainWindow->close();
			break;
			case sf::Keyboard::Q:
				bKeyDown_TurnLeft = true;
				break;
			case sf::Keyboard::E:
				bKeyDown_TurnRight = true;
				break;
			case sf::Keyboard::W:
				bKeyDown_MoveForward = true;
				break;
			case sf::Keyboard::S:
				bKeyDown_MoveBack = true;
				break;
			case sf::Keyboard::A:
				bKeyDown_MoveLeft = true;
				break;
			case sf::Keyboard::D:
				bKeyDown_MoveRight = true;
				break;

			default:
				break;
		} // switch(k)
		break;
	default:
		break;
	} // switch(mGameState)
}


/*
mInputKeyUp():
	Handles key up input events

Params:
	k = SFML key pressed
*/
void Game::mInputKeyUp(sf::Keyboard::Key k) {
	switch (mGameState) {
	case Game::__eGAMESTATE::__MAINMENU:
		break;
	case Game::__eGAMESTATE::__MAINVIEW:
		switch (k) {
			case sf::Keyboard::Q:
				bKeyDown_TurnLeft = false;
				break;
			case sf::Keyboard::E:
				bKeyDown_TurnRight = false;
				break;
			case sf::Keyboard::W:
				bKeyDown_MoveForward = false;
				break;
			case sf::Keyboard::S:
				bKeyDown_MoveBack = false;
				break;
			case sf::Keyboard::A:
				bKeyDown_MoveLeft = false;
				break;
			case sf::Keyboard::D:
				bKeyDown_MoveRight = false;
				break;
			case sf::Keyboard::F5:
				Reset();
				break;
			default:
				break;
		} // switch(k)
		break;
	default:
		break;
	} // switch(mGameState)
}



/*
mMouseMoved():
	Processes a mouse move event

Params:
	x,y - The new x,y position of the mouse cursor relative to our window
*/
void Game::mMouseMoved(int x, int y) {

}


/*
mMouseButtonPressed():
	Process a mouse button event

Params:
	b - The mouse button that was pressed
*/
void Game::mMouseButtonPressed(sf::Mouse::Button b) {
	int x = sf::Mouse::getPosition(*mainWindow).x;
	int y = sf::Mouse::getPosition(*mainWindow).y;

	// Correct for window scaling
	x = static_cast<int>(x / mScaleFactorX);
	y = static_cast<int>(y / mScaleFactorY);

	switch (mGameState) {
	case Game::__eGAMESTATE::__MAINMENU:
		break;
	case Game::__eGAMESTATE::__MAINVIEW:
		break;
	default:
		break;
	} // switch(mGameState)
}




/*
mHandleInputStatus():
	Checks stauts of keys and buttons and processes them, to be called every game tick
*/
void Game::mHandleInputStatus() {
	int x = sf::Mouse::getPosition(*mainWindow).x;
	int y = sf::Mouse::getPosition(*mainWindow).y;
	int dx;
	int dy;

	float newx, newy;
	float movement;


	switch (mGameState) {
		case Game::__eGAMESTATE::__MAINMENU:
			break; // case Game::__eGAMESTATE::__MAINMENU:
		case Game::__eGAMESTATE::__MAINVIEW:
			// Process keys
			if (bKeyDown_TurnLeft) {
				mPlayers[0].SetHeading(mPlayers[0].GetHeading() - TURN_SPEED);
			}

			if (bKeyDown_TurnRight) {
				mPlayers[0].SetHeading(mPlayers[0].GetHeading() + TURN_SPEED);
			}

			// Test for diagonal movement. If no correction is applied, then diagonal
			// movement ends up faster than just moving straight.
			if ((bKeyDown_MoveForward || bKeyDown_MoveBack) && (bKeyDown_MoveLeft || bKeyDown_MoveRight)) {
				movement = MOVEMENT_SPEED / __SQRT2__;
			}
			else {
				movement = MOVEMENT_SPEED;
			}

			if (bKeyDown_MoveForward) {
				// Update player position based on current facing
				newx = mPlayers[0].GetPositionX() + (sin(mPlayers[0].GetHeading() * __PI__ / 180.0f)) * movement;
				newy = mPlayers[0].GetPositionY() - (cos(mPlayers[0].GetHeading() * __PI__ / 180.0f)) * movement;
				mPlayers[0].Move(newx, newy, &LevelData, CUBE_SIZE);
			}
			else if (bKeyDown_MoveBack) {
				// Update player position based on current facing
				newx = mPlayers[0].GetPositionX() - (sin(mPlayers[0].GetHeading() * __PI__ / 180.0f)) * movement;
				newy = mPlayers[0].GetPositionY() + (cos(mPlayers[0].GetHeading() * __PI__ / 180.0f)) * movement;
				mPlayers[0].Move(newx, newy, &LevelData, CUBE_SIZE);
			}

			if (bKeyDown_MoveLeft) {
				// Update player position based on current facing
				newx = mPlayers[0].GetPositionX() + (sin((mPlayers[0].GetHeading() - 90.0f) * __PI__ / 180.0f)) * movement;
				newy = mPlayers[0].GetPositionY() - (cos((mPlayers[0].GetHeading() - 90.0f)* __PI__ / 180.0f)) * movement;
				mPlayers[0].Move(newx, newy, &LevelData, CUBE_SIZE);
			}
			else if (bKeyDown_MoveRight) {
				// Update player position based on current facing
				newx = mPlayers[0].GetPositionX() + (sin((mPlayers[0].GetHeading() + 90.0f) * __PI__ / 180.0f)) * movement;
				newy = mPlayers[0].GetPositionY() - (cos((mPlayers[0].GetHeading() + 90.0f)* __PI__ / 180.0f)) * movement;
				mPlayers[0].Move(newx, newy, &LevelData, CUBE_SIZE);
			}


			// Process mouse
			// Test for mouse buttons
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {

			}
			else {

			}


			// Get delta mouse movement
			dx = x - windowCenterX;
			dy = y - windowCenterY;

			// Adjust the player's heading and pitch based on the mouse sensitivity and mouse position change
			mPlayers[0].SetHeading(mPlayers[0].GetHeading() + dx * mouseSensitivity);
			mPlayers[0].SetPitch(mPlayers[0].GetPitch() + dy * mouseSensitivity);

			// Reset mouse cursor to center of screen	
			sf::Mouse::setPosition(
				{ static_cast<int>(windowCenterX),
				  static_cast<int>(windowCenterY)
				}, *mainWindow
			);

			break; // case Game::__eGAMESTATE::__MAINVIEW


		default:
			break;
	} // switch(mGameState)
}
