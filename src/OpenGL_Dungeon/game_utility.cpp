/*
  File: game_utility.cpp
  Description: Game utility functions
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
*/

#include "game.h"
#include <random>


/*
mGetRandomInt():
	Generates a random number

Params:
	low, high - A range of ints between which a number will be generated, inclusive

Returns:
	A randomly generated int
*/
int Game::mGetRandomInt(int low, int high) {
	if (low > high) return 0; // Sanity check

	std::uniform_int_distribution<int> unii(low, high);

	return(unii(*rng));
}


/*
mVFOVtoHFOV():
	Converts a vertical FOV value to horizontal

Params:
	vfov - Vertical FOV in degrees to be converted
	sizex, sizey - The size of the screen or render surface

Returns:
	The horizontal FOV in degrees
*/
float Game::mVFOVtoHFOV(float vfov, float sizex, float sizey) {
	float hfov;

	hfov = 2 * atan((0.5f * sizex) / (0.5f * sizey / tan(__DEG2RAD(vfov) / 2.0f)));
	return(__RAD2DEG(hfov));
}

