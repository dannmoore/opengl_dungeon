# OpenGL_Dungeon


![animated](https://gitlab.com/dannmoore/opengl_dungeon/raw/master/src/media/animated.gif)


### Overview:

OpenGL_Dungeon is a simple 3D engine in the style of early 90's raycast engines.  It generates a random dungeon for the player to move around in.  OpenGL_Dungeon is written in C++ with SFML 2.5.1 and OpenGL using Visual Studio 2017.



### Instructions:

At the start of the application a random dungeon will already be created.  Press F5 at any time to generate a new random dungeon.  Use the WASD keys to move.  Use Q and E to turn left and right.  Use the mouse to turn left and right, or to look up and down.  Press ESC at any time to quit the application.



### Legal:
Copyright (C) 2024 Dann Moore.  All rights reserved.

Text font used is Copyright (c) 2016 VileR, licensed under Creative Commons Attribution-ShareAlike 4.0 International License, as originally found at https://int10h.org/oldschool-pc-fonts/




